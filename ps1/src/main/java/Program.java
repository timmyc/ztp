interface IPolaczenie {
    char get(int indeks);
    void set(int indeks, char c);
    int length();
}
class Baza {
    private char[] tab = new char[100];

    private static Baza instance = null;

    private Baza() {    }

    private static Baza getBaza() {
        return instance == null ? new Baza() : instance;
    }

    public static IPolaczenie getPolaczenie() {
        return Polaczenie.getInstance();
    }
    private static class Polaczenie implements IPolaczenie {
        private Baza baza = getBaza();

        private static int counter = 0;

        private static Polaczenie[] polaczenia = {
                new Polaczenie(),
                new Polaczenie(),
                new Polaczenie()
        };

        public static IPolaczenie getInstance() {
            counter = (counter + 1) % 3;
            return polaczenia[counter];
        }
        public char get(int indeks) {
            return baza.tab[indeks];
        }
        public void set(int indeks, char c) {
            baza.tab[indeks] = c;
        }
        public int length() {
            return baza.tab.length;
        }
    }
}



public class Program {
}
