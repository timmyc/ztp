import org.junit.Assert;
import org.junit.Test;

public class TestClass {

    @Test
    public void Test1() {
        IPolaczenie test1 = Baza.getPolaczenie();
        IPolaczenie test2 = Baza.getPolaczenie();
        IPolaczenie test3 = Baza.getPolaczenie();
        IPolaczenie test4 = Baza.getPolaczenie();

        test1.set(5, 'X');

        Assert.assertEquals(test4.get(5), 'X');
    }

    @Test
    public void Test2() {
        IPolaczenie test1 = Baza.getPolaczenie();
        IPolaczenie test2 = Baza.getPolaczenie();
        IPolaczenie test3 = Baza.getPolaczenie();
        IPolaczenie test4 = Baza.getPolaczenie();
        IPolaczenie test5 = Baza.getPolaczenie();

        test2.set(3, 'Y');

        Assert.assertEquals(test5.get(3), 'Y');
    }

}
